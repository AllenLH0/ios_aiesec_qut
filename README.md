# Overview
This project is a web-based iOS app for AIESEC in QUT 2015 by using Ionic. The scope involves both front-end app interface and backend management system. More details will be added throughout the development process.

# Versions
More details will be updated soon.

# Requirements
More details will be updated soon.

# Demo
More details will be updated soon.

# Download
More details will be updated soon.

# Installation
More details will be updated soon.

# Questions
Feel free to contact me with any questions!
You can reach me at al.allen.leung@gmail.com
